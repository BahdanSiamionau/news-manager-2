package com.epam.news2.backing;

import com.epam.news2.model.News;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
public final class NewsBean implements Serializable {
 
	private static final long serialVersionUID = 1L;
 
	private List<News> newsList = new ArrayList<News>();
	private News currentNews = new News();
	private Map<Integer, Boolean> selectedNews = new HashMap<Integer, Boolean>();

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}
	
	public News getCurrentNews() {
		return currentNews;
	}

	public void setCurrentNews(News currentNews) {
		this.currentNews = currentNews;
	}
	
	public Map<Integer, Boolean> getSelectedNews() {
		return selectedNews;
	}

	public void setSelectedNews(Map<Integer, Boolean> selectedNews) {
		this.selectedNews = selectedNews;
	}
}