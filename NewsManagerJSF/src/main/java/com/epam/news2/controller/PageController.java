package com.epam.news2.controller;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.epam.news2.backing.NewsBean;
import com.epam.news2.dao.INewsDAO;
import com.epam.news2.exception.NewsManagerException;
import com.epam.news2.model.News;
import com.epam.news2.util.NewsUtilities;

public final class PageController implements Serializable {
 
	private static final long serialVersionUID = 1L;
	private static final String NEWS = "news";
	private static final String VIEW = "view";
	private static final String EDIT = "edit";
	private static final String IS_ADD_PAGE = "isAddPage";
	private static final String IS_EDIT_PAGE = "isEditPage";
	private static final String REDIRECT = "?faces-redirect=true";
	private static final Logger logger = Logger.getLogger(PageController.class);
	
	private NewsBean newsBean;
	private INewsDAO newsDAO;
	private String previousPage = "news";
	
	@PostConstruct
	public void init() {
		showNews();
	}

	public String showNews() {
		try {
			newsBean.setNewsList(newsDAO.getNewsList());
			newsBean.setSelectedNews(new HashMap<Integer, Boolean>());
			previousPage = NEWS;
		} catch (NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
	    return NEWS; 
	}
	
	public String addNews() {
		setPageTypeToAdd(true);
		News news = new News();
		news.setDate(new Date(System.currentTimeMillis()));
		newsBean.setCurrentNews(news);
	    return EDIT; 
	}
	
	public String editNews() {
		setPageTypeToAdd(false);
		return EDIT; 
	}
	
	public String editNews(int id) {
		setPageTypeToAdd(false);
		try {
			newsBean.setCurrentNews(newsDAO.getCurrentNews(id));
		} catch (NumberFormatException | NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
	    return EDIT; 
	}
	
	public String viewNews(int id) {
		try {
			newsBean.setCurrentNews(newsDAO.getCurrentNews(id));
			newsBean.setSelectedNews(new HashMap<Integer, Boolean>());
			newsBean.getSelectedNews().put(id, true);
			previousPage = VIEW;
		} catch (NumberFormatException | NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
		return VIEW;
	}
	
	public String saveNews() {
		try {
			newsDAO.addNews(newsBean.getCurrentNews());
		} catch (NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
		return viewNews(newsBean.getCurrentNews().getId()) + REDIRECT;
	}
	
	public String updateNews() {
		try {
			newsDAO.editNews(newsBean.getCurrentNews());
		} catch (NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
		return viewNews(newsBean.getCurrentNews().getId());
	}
	
	public String deleteNews() {
		try {
			List<Integer> listToDelete = new ArrayList<Integer>();
			for (Integer id : newsBean.getSelectedNews().keySet()) {
				if(newsBean.getSelectedNews().get(id)) {
					listToDelete.add(id);
				}
			}
			Integer[] arrayToDelete;
			if (listToDelete.isEmpty()) {
				arrayToDelete = new Integer[1];
				arrayToDelete[0] = newsBean.getCurrentNews().getId();
				newsDAO.removeNews(arrayToDelete);
			} else {
				arrayToDelete = new Integer[listToDelete.size()];
				newsDAO.removeNews(listToDelete.toArray(arrayToDelete));
			}
		} catch (NewsManagerException ex) {
			logger.error(ex.getMessage());
		}
		return showNews() + REDIRECT;
	}
	
	public String getMessagesAsJson() {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		return NewsUtilities.getMessagesAsJson(locale);
	}
	
	private void setPageTypeToAdd(boolean isAddPage) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(
				IS_ADD_PAGE, isAddPage ? true : false);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(
				IS_EDIT_PAGE, isAddPage ? false : true);
	}

	public NewsBean getNewsBean() {
		return newsBean;
	}

	public void setNewsBean(NewsBean newsBean) {
		this.newsBean = newsBean;
	}
	
	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}
}