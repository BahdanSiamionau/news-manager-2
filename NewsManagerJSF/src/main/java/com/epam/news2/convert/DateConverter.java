package com.epam.news2.convert;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import com.epam.news2.resource.NewsConstant;

@FacesConverter("com.epam.news2.convert.DateConverter")
public final class DateConverter implements Converter {
	
	private static final String FORMAT_DATE = "format.date";
	private static final Logger logger = Logger.getLogger(DateConverter.class);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(getDatePattern(context));
		try {
			return new Date(dateFormat.parse(value).getTime());
		} catch (ParseException ex) {
			logger.error(ex.getMessage());
		}
		return new Date(0);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Date date = (Date) value;
		SimpleDateFormat dateFormat = new SimpleDateFormat(getDatePattern(context));
		return dateFormat.format(date);
	}

	private String getDatePattern(FacesContext context) {
		ResourceBundle messages = ResourceBundle.getBundle(NewsConstant.MESSAGES, context.getViewRoot().getLocale());
		return messages.getString(FORMAT_DATE);
	}
}
