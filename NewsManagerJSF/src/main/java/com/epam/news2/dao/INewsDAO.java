package com.epam.news2.dao;

import java.util.ArrayList;

import com.epam.news2.model.News;
import com.epam.news2.exception.NewsManagerException;


public interface INewsDAO {
		
	public ArrayList <News> getNewsList() throws NewsManagerException;
	public News getCurrentNews(int id) throws NewsManagerException;
	public void addNews(News news) throws NewsManagerException;
	public void editNews(News news) throws NewsManagerException;
	public void removeNews(Integer[] selectedItems) throws NewsManagerException;
}