package com.epam.news2.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.epam.news2.dao.pool.Connector;
import com.epam.news2.dao.pool.ConnectorPool;
import com.epam.news2.exception.NewsManagerException;
import com.epam.news2.model.News;


public final class JDBCNewsDAO implements INewsDAO {

	private static final String ID = "ID";
	private static final String TITLE = "TITLE";
	private static final String DATE = "NEWS_DATE";
	private static final String BRIEF = "BRIEF";
	private static final String CONTENT = "CONTENT";
	private static final String SQL_SELECT_NEWS_ALL = "select * from NEWSTABLE order by NEWS_DATE desc";
	private static final String SQL_SELECT_NEWS = "select * from NEWSTABLE where ID=?";
	private static final String SQL_INSERT_NEWS = "insert into NEWSTABLE values (null, ?, ?, ?, ?)";
	private static final String SQL_UPDATE_NEWS = "update NEWSTABLE set TITLE=?, NEWS_DATE=?, BRIEF=?, CONTENT=? WHERE ID=?";
	private static final String SQL_REMOVE_NEWS = "delete from NEWSTABLE where ID in (";
	private static final String SQL_SEQUENCE_GET_CURVAL = "select NEWS_INCREMENT.CURRVAL from DUAL";
	private static final Logger logger = Logger.getLogger(JDBCNewsDAO.class);
	
	private ConnectorPool pool;
	
	public ArrayList <News> getNewsList() throws NewsManagerException {
		ArrayList <News> newsList = new ArrayList <News>();
		try (Connector connector = pool.take()) {
			Statement statement = connector.getStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEWS_ALL);
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getInt(ID));
				news.setTitle(resultSet.getString(TITLE));
				news.setDate(resultSet.getDate(DATE));
				news.setBrief(resultSet.getString(BRIEF));
				news.setContent(resultSet.getString(CONTENT));
				newsList.add(news);
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
		return newsList;
	}

	public News getCurrentNews(int id) throws NewsManagerException {
		News currentNews = new News();
		try (Connector connector = pool.take()) {
			PreparedStatement preparedStatement = connector.getPreparedStatement(SQL_SELECT_NEWS);
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				currentNews = new News();
				currentNews.setId(resultSet.getInt(ID));
				currentNews.setTitle(resultSet.getString(TITLE));
				currentNews.setDate(resultSet.getDate(DATE));
				currentNews.setBrief(resultSet.getString(BRIEF));
				currentNews.setContent(resultSet.getString(CONTENT));
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
		return currentNews;
	}

	public void addNews(News news) throws NewsManagerException {
		try (Connector connector = pool.take()) {
			PreparedStatement pStatement = connector.getPreparedStatement(SQL_INSERT_NEWS);
			pStatement.setString(1, news.getTitle());
			pStatement.setDate(2, news.getDate());
			pStatement.setString(3, news.getBrief());
			pStatement.setString(4, news.getContent());
			pStatement.executeUpdate();
			
			Statement statement = connector.getStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SEQUENCE_GET_CURVAL);
			if (resultSet.next()) {
				news.setId(resultSet.getInt(1));
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}

	public void editNews(News news) throws NewsManagerException {
		try (Connector connector = pool.take()) {
			PreparedStatement statement = connector.getPreparedStatement(SQL_UPDATE_NEWS);
			statement.setString(1, news.getTitle());
			statement.setDate(2, news.getDate());
			statement.setString(3, news.getBrief());
			statement.setString(4, news.getContent());
			statement.setInt(5, news.getId());
			statement.executeUpdate(); 
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
	}

	public void removeNews(Integer[] selectedItems) throws NewsManagerException {
		try (Connector connector = pool.take()) {
			StringBuffer query = new StringBuffer(SQL_REMOVE_NEWS);
			for (int i = 0; i < selectedItems.length; i++) {
				if (i != 0) {
					query.append(',');
				}
				query.append('?');	
			}
			query.append(')');
			PreparedStatement statement = connector.getPreparedStatement(query.toString());
			for (int i = 0; i < selectedItems.length; i++) {
				statement.setInt(i + 1, selectedItems[i]);
			}
			statement.executeUpdate();
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	
	public ConnectorPool getPool() {
		return pool;
	}
	
	public void setPool (ConnectorPool pool) {
		this.pool = pool;
	}
} 
