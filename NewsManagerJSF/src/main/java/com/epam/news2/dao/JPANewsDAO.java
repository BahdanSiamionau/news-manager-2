package com.epam.news2.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.epam.news2.exception.NewsManagerException;
import com.epam.news2.model.News;


public final class JPANewsDAO implements INewsDAO {
	
	private static final String SELECT_ALL = "select_all";
	private static final String DELETE = "delete";
	private static final String ID = "ID";
	
	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList <News> getNewsList() throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			Query query = entityManager.createNamedQuery(SELECT_ALL);
			List<News> list = query.getResultList();
			entityManager.getTransaction().commit();
			return new ArrayList <News> (list);
		} finally {
			entityManager.close();
		}
	}

	@Override
	public News getCurrentNews(int id) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			entityManager.getTransaction().commit();
			return entityManager.find(News.class, id);
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void addNews(News news) throws NewsManagerException {
		news.setId(0);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			entityManager.persist(news);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void editNews(News news) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			entityManager.merge(news);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void removeNews(Integer[] selectedItems) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			Query query = entityManager.createNamedQuery(DELETE);
			query.setParameter(ID, Arrays.asList(selectedItems));
			query.executeUpdate();
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
}
