package com.epam.news2.exception;

public final class NewsManagerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NewsManagerException(String msg) {
		super(msg);
	}
	
	public NewsManagerException(Exception ex) {
		super(ex);
	}
}
