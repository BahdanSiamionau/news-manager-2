package com.epam.news2.resource;

public final class NewsConstant {

	public static final String MESSAGE = "message";
	public static final String MESSAGES = "messages";
	
	private NewsConstant() {
		
	}
}
