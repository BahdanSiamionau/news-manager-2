package com.epam.news2.util;

import java.util.Locale;
import java.util.ResourceBundle;

import com.epam.news2.resource.NewsConstant;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public final class NewsUtilities {
	
	private NewsUtilities() {
		
	}

	public static String getMessagesAsJson(Locale locale) {
		ResourceBundle messages = ResourceBundle.getBundle(NewsConstant.MESSAGES, locale);
		JsonObject json = new JsonObject();
		for (String key : messages.keySet()) {
			json.addProperty(key, messages.getString(key));
		}
        return new Gson().toJson(json);
	}
}
