package com.epam.news2.validation;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.epam.news2.resource.NewsConstant;

@FacesValidator("com.epam.news2.validation.DateValidator")
public final class DateValidator implements Validator {
	
	private static final String DEFAULT_DATE = "1970-01-01";

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		if (DEFAULT_DATE.equals(value.toString())) {
			FacesMessage msg = new FacesMessage((String)component.getAttributes().get(NewsConstant.MESSAGE));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
