package com.epam.news2.validation;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.epam.news2.resource.NewsConstant;

@FacesValidator("com.epam.news2.validation.TitleValidator")
public final class TitleValidator implements Validator {
	
	private static final int MAX_LENGTH = 120;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		if (value.toString().length() > MAX_LENGTH) {
			FacesMessage msg = new FacesMessage((String)component.getAttributes().get(NewsConstant.MESSAGE));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}